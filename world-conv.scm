;#lang scheme
(require "scripts/graph.ss")
(require "scripts/naked.ss")

(define infile "../game-client/htdocs/state/game.graph")
(define outfile "../game-client/htdocs/state/game2.graph")

(define l
  (let* ((f (open-input-file infile))
         (r (read f)))
    (close-input-port f)
    r))

(define (conv-object object)
  (naked-object
   object "I haven't been described yet" '() '()))

(define (conv-objects objects)
  (map
   (lambda (object)
     (conv-object object))
   objects))

(define (conv-node node)
  (list
   (node-name node)
   (list 
    (naked-node-info node)
    (conv-objects (naked-node-objects node)))))
  

(define (conv-nodes nodes)
  (map 
   (lambda (node)
     (conv-node node))
   nodes))

(let ((f (open-output-file outfile #:exists 'replace))
      (o (list (conv-nodes (car l)) (cadr l))))
  (display o)(newline)
  (write o f)
  (close-output-port f))